# Birra del Parco - Know How
Questa repository raccoglie tutta la ricerca e la conoscenza acquisita da Birra del Parco nel processo di sviluppo e trasformazione aziendale avviato in seguito alla pandemia di COVID-19.

## [Incentivi](./incentivi/)
Una documentazione approfondita su quelli che sono i **sostegni all'attività di impresa** a livello **europeo, italiano e regionale** per le PMI italiane.

## [Processi](./processi/)
Idee e piani di sviluppo per **migliorare l'efficienza dei metodi e dei processi aziendali**, con particolare attenzione sullo **sviluppo tecnologico e digitale**.

## [Altro](./altro/)
Raccolta generica di informazioni utili o interessanti per lo sviluppo aziendale.
