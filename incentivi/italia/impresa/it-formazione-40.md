# Credito di imposta per formazione 4.0

- [Descrizione](#descrizione)
- [Possibili progetti finanziabili](#possibili-progetti-finanziabili)
- [Agevolazioni](#agevolazioni)

## Descrizione
Il **[credito di imposta per formazione 4.0](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-formazione)** è volto a stimolare gli investimenti delle imprese nella formazione del personale sulle materie aventi ad oggetto le tecnologie rilevanti per la trasformazione tecnologica e digitale delle imprese.

La misura è di tipo: **Credito di imposta**

Periodo di validità: **la misura è stata estesa a tutto il 2020**

## Possibili progetti finanziabili
I progetti finanziabili rientrano negli ambiti definiti dal piano per l'industria 4.0:
- big data e analisi dei dati
- cloud e fog computing
- cyber security
- sistemi cyber-fisici
- prototipazione rapida
- sistemi di visualizzazione e realtà aumentata
- robotica avanzata e collaborativa
- interfaccia uomo macchina
- manifattura additiva
- internet delle cose e delle macchine
- integrazione digitale dei processi aziendali

Le attività formative dovranno inoltre riguardare i seguenti ambiti:
- vendita e marketing
- informatica e tecniche
- tecnologie di produzione

## Agevolazioni
- 50% delle spese ammissibili e nel limite massimo annuale di €. 300.000 per le piccole imprese
- 40% delle spese ammissibili nel limite massimo annuale di €. 250.000 per le medie imprese
- 30% delle spese ammissibili nel limite massimo annuale di €. 250.000 le grandi imprese

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *5 novembre 2020*