# Digital transformation

- [Descrizione](#descrizione)
- [Possibili progetti finanziabili](#possibili-progetti-finanziabili)
- [Agevolazioni](#agevolazioni)
- [Limitazioni](#limitazioni)

## Descrizione
Il programma per la **[Digital transformation](https://www.mise.gov.it/index.php/it/incentivi/impresa/digital-transformation)** è finalizzato a sostenere la trasformazione tecnologica e digitale dei processi produttivi delle micro, piccole e medie imprese attraverso la realizzazione di progetti diretti all'implementazione delle tecnologie abilitanti individuate nel Piano Nazionale Impresa 4.0 nonché di altre tecnologie relative a soluzioni tecnologiche digitali di filiera.

La misura è di tipo: **Finanziamento / Aiuto al credito**

Apertura domande: **ore 12.00 del 15 dicembre 2020**

## Possibili progetti finanziabili
I progetti ammissibili alle agevolazioni devono essere diretti alla trasformazione tecnologica e digitale dei processi produttivi dei soggetti proponenti mediante l’implementazione di:

- **Tecnologie abilitanti individuate dal Piano nazionale impresa 4.0** (advanced manufacturing solutions, addittive manufacturing, realtà aumentata, simulation, integrazione orizzontale e verticale, industrial internet, cloud, cybersecurity, big data e analytics)
- **Tecnologie relative a soluzioni tecnologiche digitali di filiera**, finalizzate:
	- all’ottimizzazione della gestione della catena di distribuzione e della gestione delle relazioni con i diversi attori
	- al software
	- alle piattaforme e applicazioni digitali per la gestione e il coordinamento della logistica con elevate caratteristiche di integrazione delle attività di servizio
	- ad altre tecnologie, quali sistemi di e-commerce, sistemi di pagamento mobile e via internet, fintech, sistemi elettronici per lo scambio di dati (electronic data interchange-EDI), geolocalizzazione, tecnologie per l’in-store customer experience, system integration applicata all’automazione dei processi, blockchain, intelligenza artificiale, internet of things
- A tal fine i progetti devono prevedere la realizzazione di:
	- attività di innovazione di processo o di innovazione dell’organizzazione, ovvero
	- investimenti

## Agevolazioni

Risorse stanziate: **€ 100 milioni**

Per entrambe le tipologie di progetto ammissibili a beneficio le agevolazioni sono concesse sulla base di una percentuale nominale dei costi e delle spese ammissibili pari al 50 percento, articolata come segue:
- 10 percento sotto forma di contributo
- 40 percento come finanziamento agevolato

## Limitazioni

I progetti di spesa devono:
- essere realizzati nell'ambito di una unità produttiva dell’impresa proponente ubicata su tutto il territorio nazionale
- prevedere un importo di spesa non inferiore a euro 50.000,00 e non superiore a 500.000,00
- essere avviati successivamente alla presentazione della domanda di accesso alle agevolazioni
- prevedere una durata non superiore a 18 mesi dalla data del provvedimento di concessione delle agevolazioni

> Su richiesta motivata dal soggetto beneficiario il Ministero si riserva la possibilità di concedere una proroga del termine di ultimazione non superiore a 6 mesi.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*