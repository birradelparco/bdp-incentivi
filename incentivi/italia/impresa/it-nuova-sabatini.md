# Nuova Sabatini

- [Descrizione](#descrizione)
- [Prospettive future](#prospettive-future)

## Descrizione
La **[Nuova Sabatini](https://www.mise.gov.it/index.php/it/incentivi/impresa/beni-strumentali-nuova-sabatini)** sostiene gli investimenti per acquistare o acquisire in leasing macchinari, attrezzature, impianti, beni strumentali ad uso produttivo e hardware, nonché software e tecnologie digitali.

La misura è di tipo: **Contributo in conto interessi**

> !!! - Aiuto al credito bancario. Da valutare dopo finanziamenti o altre forme di sostegno.

## Prospettive future

Prospettive future legge bilancio 2020:
- 105 milioni di euro per il 2020;
- 97 milioni per gli anni dal 2021 al 2024;
- 47 milioni per il 2025.

Vantaggi ulteriori per macchinari a basso impatto ambientale

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*