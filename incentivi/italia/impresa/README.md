# Incentivi - Italia / Impresa

## [Transizione 4.0](https://www.mise.gov.it/index.php/it/transizione40)

### [Credito di imposta per investimenti in beni strumentali](it-investimenti-in-beni-strumentali.md) - PRINCIPALE
Il **[credito di imposta per investimenti in beni strumentali](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-beni-strumentali)** è una misura volta a supportare e incentivare le imprese che investono in beni strumentali nuovi, materiali e immateriali, funzionali alla trasformazione tecnologica e digitale dei processi produttivi destinati a strutture produttive ubicate nel territorio dello Stato.

### [Ricerca, Sviluppo e Innovazione](it-ricerca-sviluppo-innovazione.md) - PRINCIPALE
Il **[Credito di imposta R&S&I](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-r-s)** si pone l’obiettivo di stimolare la spesa privata in Ricerca, Sviluppo e Innovazione tecnologica per sostenere la competitività delle imprese e per favorirne i processi di transizione digitale e nell’ambito dell’economia circolare e della sostenibilità ambientale.

### [Credito di imposta per formazione 4.0](it-formazione-40.md) - PRINCIPALE
Il **[credito di imposta per formazione 4.0](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-formazione)** è volto a stimolare gli investimenti delle imprese nella formazione del personale sulle materie aventi ad oggetto le tecnologie rilevanti per la trasformazione tecnologica e digitale delle imprese.

### [Digital transformation](it-digital-transformation.md)
Il programma per la **[Digital transformation](https://www.mise.gov.it/index.php/it/incentivi/impresa/digital-transformation)** è finalizzato a sostenere la trasformazione tecnologica e digitale dei processi produttivi delle micro, piccole e medie imprese attraverso la realizzazione di progetti diretti all'implementazione delle tecnologie abilitanti individuate nel Piano Nazionale Impresa 4.0 nonché di altre tecnologie relative a soluzioni tecnologiche digitali di filiera.

### [Nuova Sabatini](it-nuova-sabatini.md)
La **[Nuova Sabatini](https://www.mise.gov.it/index.php/it/incentivi/impresa/beni-strumentali-nuova-sabatini)** sostiene gli investimenti per acquistare o acquisire in leasing macchinari, attrezzature, impianti, beni strumentali ad uso produttivo e hardware, nonché software e tecnologie digitali.

### [Voucher per consulenza in innovazione](it-voucher-per-consulenza-in-innovazione.md)
Il **[Voucher per consulenza in innovazione](https://www.mise.gov.it/index.php/it/incentivi/impresa/voucher-consulenza-innovazione)** sostiene i processi di trasformazione tecnologica e digitale delle PMI e delle reti di impresa di tutto il territorio nazionale attraverso l’introduzione in azienda di figure manageriali in grado di implementare le tecnologie abilitanti previste dal Piano Nazionale Impresa 4.0, nonché di ammodernare gli assetti gestionali e organizzativi dell’impresa, compreso l’accesso ai mercati finanziari e dei capitali.

## Altro

### [Patent Box](it-patent-box.md)
Il **[Patent Box](https://www.mise.gov.it/index.php/it/incentivi/impresa/patent-box)** è un regime opzionale di tassazione per i redditi d'impresa derivanti dall’utilizzo di software protetto da copyright, di brevetti industriali, di disegni e modelli, nonché di processi, formule e informazioni relativi ad esperienze acquisite nel campo industriale, commerciale o scientifico giuridicamente tutelabili.

### [Iper e superammortamento](it-iper-superammortamento.md) - SCADUTO
**Vedere ->** [Credito d’imposta per investimenti in beni strumentali](it-investimenti-in-beni-strumentali.md)
