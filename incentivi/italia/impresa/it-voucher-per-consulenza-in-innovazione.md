# Voucher per consulenza in innovazione

- [Descrizione](#descrizione)
- [Spese ammissibili](#spese-ammissibili)
- [Agevolazioni](#agevolazioni)

## Descrizione
Il **[Voucher per consulenza in innovazione](https://www.mise.gov.it/index.php/it/incentivi/impresa/voucher-consulenza-innovazione)** sostiene i processi di trasformazione tecnologica e digitale delle PMI e delle reti di impresa di tutto il territorio nazionale attraverso l’introduzione in azienda di figure manageriali in grado di implementare le tecnologie abilitanti previste dal Piano Nazionale Impresa 4.0, nonché di ammodernare gli assetti gestionali e organizzativi dell’impresa, compreso l’accesso ai mercati finanziari e dei capitali.

La misura è di tipo: **Voucher**

> ??? - Guardare la questione delle figure/aziende riconosciute come manager dal MISE

## Spese ammissibili
- big data e analisi dei dati;
- cloud, fog e quantum computing;
- cyber security;
- integrazione delle tecnologie della Next Production Revolution (NPR) nei processi aziendali, anche e con particolare riguardo alle produzioni di natura tradizionale;
- simulazione e sistemi cyber-fisici;
- prototipazione rapida;
- sistemi di visualizzazione, realtà virtuale (RV) e realtà aumentata (RA);
- robotica avanzata e collaborativa;
- interfaccia uomo-macchina;
- manifattura additiva e stampa tridimensionale;
- internet delle cose e delle macchine;
- integrazione e sviluppo digitale dei processi aziendali;
- programmi di digital eting, quali processi trasformativi e abilitanti per l’innovazione di tutti i processi di valorizzazione di marchi e segni distintivi (c.d. “branding”) e sviluppo commerciale verso mercati;
- programmi di open innovation.

## Agevolazioni
L’agevolazione è costituita da un contributo in forma di **voucher concedibile in regime “de minimis”** ai sensi del Regolamento (UE) n. 1407/2013. Il contributo massimo concedibile è differenziato in funzione della tipologia di beneficiario:
- **Micro e piccole**: contributo pari al 50% dei costi sostenuti fino ad un massimo di 40 mila euro
- **Medie imprese**: contributo pari al 30% dei costi sostenuti fino ad un massimo di 25 mila euro
- **Reti di imprese**: contributo pari al 50% dei costi sostenuti fino ad un massimo di 80 mila euro

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*