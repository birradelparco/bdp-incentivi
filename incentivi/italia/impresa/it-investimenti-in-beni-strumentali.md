# Credito di imposta per investimenti in beni strumentali

- [Descrizione](#descrizione)

## Descrizione
Il **[credito di imposta per investimenti in beni strumentali](https://www.mise.gov.it/index.php/it/incentivi/impresa/credito-d-imposta-beni-strumentali)** è una misura volta a supportare e incentivare le imprese che investono in beni strumentali nuovi, materiali e immateriali, funzionali alla trasformazione tecnologica e digitale dei processi produttivi destinati a strutture produttive ubicate nel territorio dello Stato.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*