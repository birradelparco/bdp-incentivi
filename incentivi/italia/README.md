# Incentivi - Italia

## [Comunicazione](./comunicazione/)
Misure volte a supportare e incentivare la **comunicazione dell'azienda** e del suo **marchio**.

## [Impresa](./impresa/)
Misure volte a supportare e sviluppare i **processi** e le **strumentazioni** delle azienda, con speciale attenzione alla **green economy** e al **digitale**.
