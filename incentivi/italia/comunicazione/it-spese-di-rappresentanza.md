# Spese di rappresentanza (viaggi, feste, eventi)

- [Descrizione](#descrizione)
- [Spese di vitto e alloggio](#spese-di-vitto-e-alloggio)
- [Leggi di riferimento](#leggi-di-riferimento)
- [Fonti](#fonti)

## Descrizione
Le spese di rappresentanza hanno il vincolo della gratuità imprescindibile. Non potrebbero essere sfruttate quindi per eventi dove si vende al pubblico. Potrebbero essere invece interessanti per gli eventi promozionali lato B2B.

Rientrano nelle spese di rappresentanza:
- Spese per **viaggi turistici**.
- Spese per **feste e intrattenimenti**.
- Spese per **banchetti e intrattenimenti**.
- Spese sostenute per **feste** in dipendenza di **fiere** e simili.
- Spese per **beni distribuiti gratuitamente** durante convegni, seminari e simili.

**Scaglioni**

| Deducibilità | Ammontare dei ricavi dell’impresa            |
|------|------------------------------------------------------|
| 1,5% | Ricavi dell’impresa da 0 a  €. 10.000.000            |
| 0,6% | Ricavi dell’impresa da €. 10.000.001 a €. 50.000.000 |
| 0,4% | Ricavi dell’impresa da €. 50.000.001 in poi          |

**Esempio**

> Se nel 2019 sono conseguiti ricavi per un ammontare pari a 60.000.000 di euro, il plafond di deducibilità delle spese di rappresentanza relativo al medesimo esercizio è pari a 430.000 euro, ottenuto dalla somma tra:
– 0,015 x 10.000.000,00 = 150.000,00;
– 0,006 x 40.000.000,00 (50.000.000,00 – 10.000.000,00) = 240.000,00;
– 0,004 x 10.000.000,00 (60.000.000,00 – 50.000.000,00) = 40.000,00.

## Spese di vitto e alloggio
Questo tipo di spese hanno la piena deducibilità.

> **???** - Verificare limite del 75%

Spese di vitto e alloggio sostenute per i clienti, anche potenziali, in occasioni di fiere, eventi, incontri o visite agli uffici/stabilimenti. Sono comprese anche le spese sostenute dall'imprenditore stesso durante trasferte per mostre, eventi, fiere e simili dove sono esposti prodotti dell'azienda o attinenti alla materia dell'azienda.

## Leggi di riferimento
Articolo 108, comma 2 del DPR n. 917/86 (TUIR)

## Fonti
- [Spese di rappresentanza](https://www.theitaliantimes.it/economia/spese-di-rappresentanza-deducibilita-reddito-detraibilita-iva_210720/)
- [Spese di rappresentanza 2](https://fiscomania.com/spese-di-rappresentanza-deducibilita/)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*