# Gadget aziendali

- [Descrizione](#descrizione)
- [Limitazioni](#limitazioni)
- [Leggi di riferimento](#leggi-di-riferimento)
- [Fonti](#fonti)

## Descrizione
Le spese per i gadget sono **deducibili** dal reddito di impresa e **detraibili** lato IVA nella misura del 100%.

## Limitazioni
- Limite di costo per gadget unitario di 50€.
- I gadget non possono essere distribuiti ai propri dipendenti.
- I gadget non possono rientrare nella classe merceologica a cui l'azienda fa riferimento.
- I gadget devono essere ceduti a titolo gratuito.

## Leggi di riferimento
Decreto legislativo 175/2014 e DPR n. 633/72

## Fonti
- [Gadget aziendali](http://colibrimagazine.it/corsi/gadget-aziendali-marketing-benefici-fiscali-quali-vantaggi/)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*