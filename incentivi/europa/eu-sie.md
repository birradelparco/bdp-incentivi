# SIE - Fondi strutturali e di investimento europei

- [Descrizione](#descrizione)
- [I 5 fondi](#i-5-fondi)
- [Gestione dei fondi](#gestione-dei-fondi)

## Descrizione
I **[Fondi SIE](https://ec.europa.eu/info/funding-tenders/funding-opportunities/funding-programmes/overview-funding-programmes/european-structural-and-investment-funds_it)** costituiscono più della metà dei fondi dell’UE. All'interno di questo programma ci sono 5 Fondi strutturali e d’investimento europei. I fondi sono gestiti congiuntamente dalla Commissione europea e dai paesi dell’UE. 

Tutti questi fondi servono a effettuare investimenti per creare posti di lavoro e un’economia e un ambiente sani e sostenibili in Europa.

I fondi SIE si concentrano su 5 settori:
- ricerca e innovazione
- tecnologie digitali
- sostenere l’economia a basse emissioni di carbonio
- gestione sostenibile delle risorse naturali
- piccole imprese

## I 5 fondi
- **[Fondo europeo di sviluppo regionale (FESR)](http://ec.europa.eu/regional_policy/en/funding/erdf/)** - che promuove uno sviluppo equilibrato nelle diverse regioni dell’UE.
- **[Fondo sociale europeo (FSE)](http://ec.europa.eu/esf/home.jsp?langId=en)** - che sostiene progetti in materia di occupazione in tutta Europa e investe nel capitale umano dell’Europa: nei lavoratori, nei giovani e in tutti coloro che cercano un lavoro.
- **[Fondo di coesione (FC)](http://ec.europa.eu/regional_policy/en/funding/cohesion-fund/)** - che finanzia i progetti nel settore dei trasporti e dell'ambiente nei paesi in cui il reddito nazionale lordo (RNL) pro capite è inferiore al 90% della media dell’UE. Nel periodo 2014-2020, si tratta di Bulgaria, Croazia, Cipro, Repubblica ceca, Estonia, Grecia, Ungheria, Lettonia, Lituania, Malta, Polonia, Portogallo, Romania, Slovacchia e Slovenia.
- **[Fondo europeo agricolo per lo sviluppo rurale (FEASR)](http://ec.europa.eu/agriculture/rural-development-2014-2020/index_en.htm)** - che si concentra sulla risoluzione di sfide specifiche cui devono far fronte le zone rurali dell’UE.
- **[Fondo europeo per gli affari marittimi e la pesca (FEAMP)](http://ec.europa.eu/fisheries/cfp/emff/index_en.htm)** - che aiuta i pescatori a utilizzare metodi di pesca sostenibili e le comunità costiere a diversificare le loro economie, migliorando la qualità della vita nelle regioni costiere europee.

## Gestione dei fondi
Tutti questi fondi sono gestiti dai paesi stessi, attraverso **[accordi di partenariato](https://ec.europa.eu/info/publications/partnership-agreements-european-structural-and-investment-funds_it)**.

Ogni paese prepara un accordo, in collaborazione con la Commissione europea, che illustra in che modo i fondi saranno utilizzati durante l’attuale periodo di finanziamento 2014-2020.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *5 novembre 2020*