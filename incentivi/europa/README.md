# Incentivi - Europa

## [Fondi SIE](eu-sie.md)
I **[Fondi SIE](https://ec.europa.eu/info/funding-tenders/funding-opportunities/funding-programmes/overview-funding-programmes/european-structural-and-investment-funds_it)** costituiscono più della metà dei fondi dell’UE. All'interno di questo programma ci sono 5 Fondi strutturali e d’investimento europei. I fondi sono gestiti congiuntamente dalla Commissione europea e dai paesi dell’UE.

## [Piano SURE](eu-sure.md)
Il **[piano SURE](https://ec.europa.eu/info/business-economy-euro/economic-and-fiscal-policy-coordination/financial-assistance-eu/funding-mechanisms-and-facilities/sure_it)** (temporary **S**upport to mitigate **U**nemployment **R**isks in an **E**mergency) è un piano a sostegno della cassa integrazione dei singoli stati.

L'Italia da questo programma riceverà in totale **€ 27,4 miliardi**.

## [Next Generation EU](eu-next-generation-eu.md)
Il **[Next Generation EU](https://ec.europa.eu/info/live-work-travel-eu/health/coronavirus-response/recovery-plan-europe/pillars-next-generation-eu_it)** è un **piano comunitario** del valore complessivo di **€ 750 miliardi** che potranno essere spesi dalle nazioni **tra la metà del 2021 ed entro la fine del 2024**.

Di questa somma l'**Italia** riceverà **≈ € 200 miliardi**. Di questi € 65 miliardi saranno a fondo perduto, mentre gli altri € 127 miliardi saranno dei prestiti a tassi agevolati.