# Next Generation EU

- [Descrizione](#descrizione)
	- [Letture fortemente consigliate](#letture-fortemente-consigliate)
- [Condizioni di accesso](#condizioni-di-accesso)
	- [4 principi guida](#4-principi-guida)
	- [Raccomandazioni specifiche per singolo Paese](#raccomandazioni-specifiche-per-singolo-paese)
	- [7 obiettivi principali](#7-obiettivi-principali)
- [Presentazione dei piani e previsioni temporali](#presentazione-dei-piani-e-previsioni-temporali)
- [Prime mosse del governo](#prime-mosse-del-governo)
- [Opinioni e analisi](#opinioni-e-analisi)
	- [Più soldi di quanti si pensi](#più-soldi-di-quanti-si-pensi)
	- [Le performance contanto](#le-performance-contano)
	- [Conseguenze della chiusura al MES](#conseguenze-della-chiusura-al-mes)
	- [Speculazioni sui fondi agli enti locali](#speculazioni-sui-fondi-agli-enti-locali)

## Descrizione
Il **[Next Generation EU](https://ec.europa.eu/info/live-work-travel-eu/health/coronavirus-response/recovery-plan-europe/pillars-next-generation-eu_it)** è un **piano comunitario** del valore complessivo di **€ 750 miliardi** che potranno essere spesi dalle nazioni **tra la metà del 2021 ed entro la fine del 2024**.

Di questa somma l'**Italia** riceverà **≈ € 200 miliardi**. Di questi € 65 miliardi saranno a fondo perduto, mentre gli altri € 127 miliardi saranno dei prestiti a tassi agevolati.

> Le quote precise non sono ancora disponibili in quanto una parte di queste verranno assegnate sulla base della diminuzione del PIL.

### Letture fortemente consigliate
- [Limes - Next Generation EU = 18 piani Marshall](https://www.limesonline.com/italia-sfida-recovery-fund-eu-conte-spesa/120713)
- [DOCUMENTO - Linee guida per la definizione del piano nazionale di ripresa e resilienza](assets/linee-essenziali-pnrr-italia.pdf)


## Condizioni di accesso
L'accesso ai fondi è subordinato al rispetto di alcune norme e principi guida.

Sebbene possano sembrare una limitazione questi principi ci sono in realtà utili per capire quali saranno gli effettivi campi in cui il governo andrà a impiegare queste risorse.

Di seguito sono riportate i tre gruppi di condizioni con i loro punti specifici.

### 4 principi guida
- **Sostenibilità ambientale**
	- Almeno il **37% della spesa** deve essere rivolto alla **green economy**
- **Produttività**
	- Almeno il **20% della spesa** deve finanziare la **transizione digitale di imprese e famiglie**
- **Equità**
	- Ridurre il gap sociale causato dal COVID
- **Stabilità macroeconomica**
	- Cercare di rendere le finanze pubbliche sostenibili

### Raccomandazioni specifiche per singolo Paese
*Raccomandazioni 2020*
![Raccomandazioni 2020](assets/raccomandazioni-2020.png)

*Raccomandazioni 2019*
![Raccomandazioni 2019](assets/raccomandazioni-2019.png)

**Approfondimento** -> [DOCUMENTO - Semestre europeo 2020: raccomandazioni specifiche per paese](assets/raccomandazioni-paese.pdf)\
**Approfondimento** -> [DOCUMENTO - Linee guida per la definizione del piano nazionale di ripresa e resilienza](assets/linee-essenziali-pnrr-italia.pdf)

### 7 obiettivi principali
- **Utilizzare più energia pulita**
	- Utilizzare tecnologie pulite adeguate alle esigenze future e accelerare lo sviluppo e l'uso delle energie rinnovabili
- **Rinnovare**
	- Migliorare l'efficienza energetica degli edifici pubblici e privati
- **Ricaricare e rifornire**
	- Promuovere energie pulite adeguate alle esigenze future per accelerare l'uso di sistemi di trasporto sostenibili, accessibili e intelligenti, stazioni di ricarica e rifornimento e l'estensioni dei trasporti pubblici
- **Collegare**
	- Estendere rapidamente i servizi veloci a banda larga a tutte le regioni e a tutte le famiglie, comprese le reti in fibra ottica e 5G
- **Modernizzare**
	- Digitalizzare la pubblica amministrazioni e i servizi pubblici, compresi i sistemi giudiziari e sanitari
- **Espandere**
	- Aumentare le capacità di cloud industriale europeo di dati e lo sviluppo dei processori più potenti, all'avanguardia e sostenibili
- **Riqualificare e migliorare le competenze**
	- Adattare i sistemi di istruzione per promuovere le competenze digitali e la formazione scolastica e professionale per tutte le età

## Presentazione dei piani e previsioni temporali
I piani che ogni stato deve presentare per accedere ai fondi vengono definiti: **[Piani nazionali di ripresa e resilienza](http://www.politicheeuropee.gov.it/it/comunicazione/approfondimenti/pnrr-approfondimento/)**.

La fascia temporale di presentazione è tra il **15 ottobre 2020** e il **30 aprile 2021**. 

> É molto probabile che il piano italiano, così come quelli di tutti gli altri stati, sarà presentato verso la fine di aprile.

Dopo il 30 aprile 2020 la **[Commissione Europea](https://ec.europa.eu/info/index_it)** avrà **60 giorni** di tempo per valutare ogni piano nazionale e verificarne la conformità con le [condizioni di accesso](#condizioni-di-accesso).

In seguito all'approvazione della Commissione, il **[Consiglio Europeo](https://www.consilium.europa.eu/it/european-council/)** avrà altre **4 settimane** per valutare ogni piano. Ogni piano avrà bisogno della [maggioranza qualificata](https://it.wikipedia.org/wiki/Maggioranza_qualificata_nell%27Unione_europea) per poter essere approvato in via definitiva.

> La procedura può risultare lunga e complessa, ma in realtà consente a ogni stato di verificare la bontà dei piani di ogni altro stato, in quanto è giusto ricordare che il Next Generation EU è un piano frutto di indebitamento comune.

## Prime mosse del governo
Sebbene come riportato nel [precedente paragrafo](#presentazione-dei-piani-e-previsioni-temporali) il termine ultimo di presentazione dei piani nazionali sia il 30 aprile 2021, il governo ha iniziato a stilare una lista di obiettivi generali e ha dato il via libera ai ministeri per la proposta di progetti specifici.

I punti qui riportati saranno molto probabilmente la direzione in cui verranno orientati i progetti e di conseguenza il rilascio dei fondi.

*Obiettivi governo*
![Obiettivi governo](assets/obiettivi-governo.png)

**Approfondimento** -> [DOCUMENTO - Linee guida per la definizione del piano nazionale di ripresa e resilienza](assets/linee-essenziali-pnrr-italia.pdf)

## Opinioni e analisi
[Limes](https://www.limesonline.com/chi-siamo) (Rivista Italiana di Geopolitica) nel suo [articolo](https://www.limesonline.com/italia-sfida-recovery-fund-eu-conte-spesa/120713) a commento degli ultimi sviluppi sul fronte Next Generation EU riporta alcune considerazioni interessanti.

### Più soldi di quanti si pensi
> Il Programma, finanziato dal bilancio comunitario e con raccolta di capitali sul mercato finanziario, vale nella sua totalità 750 miliardi di euro di cui 390 fondo perduto e 360 miliardi in prestiti (dunque a debito). Comprendere l’origine dei fondi è importante in quanto la quota a prestito dovrà finanziare progetti con flussi di cassa che garantiscano la restituzione del debito… Come già anticipato, l’ammontare del Programma si aggiunge al bilancio comunitario dei prossimi 7 anni (2021 – 2027) del valore di 1.074,3 miliardi di euro.

### Le performance contano
> **Il Programma è “Performance-based”**: le erogazioni sono legate al raggiungimento dei risultati e target definiti nel piano con un pre-finanziamento del 10%. Ergo, no milestones, no money. Una bella sfida per l’Italia e tutti i presidenti e primi ministri, in quanto per ricevere i pagamenti non è sufficiente solo spendere e realizzare le attività (per esempio, installare cappotti termici) ma raggiungere i risultati concordati nei piani nazionali, come la riduzione delle emissioni di CO2 di una certa percentuale.

### Conseguenze della chiusura al MES

> **La lista dei progetti Italiani: niente MES e si vede…** La bozza dei 557 progetti Italiani è disponibile dal 30 settembre 2020 in rete. Non entriamo nel merito dei progetti in quanto la lista è provvisoria e sarà modificata in corso d’opera. Tuttavia, la fotografia attuale pone delle domande:
> - **Il valore totale dei progetti à di circa 647 miliardi di euro**, più del triplo rispetto ai 207 miliardi di euro. I progetti saranno selezionati incrociando quattro priorità: rispondenza alle linee guida, priorità politica, livello di preparazione e fattibilità economica e finanziaria. I progetti che saranno finanziati con i prestiti dovranno dimostrare di ripagare il debito. Non abbiamo elementi per capire cosa c’è dietro il titolo di un progetto da 20 miliardi, se solo un nome o un progetto che risponde ai criteri d’istruttoria. Tuttavia, qualche dubbio lo abbiamo;
> - **21 amministrazioni centrali – compresa la Guardia di finanza – hanno presentato progetti**. In ordine prioritario di valore, ministero della salute (31,8%), Mise(25%), innovazione (5,6%) trasporti (3,1%) e Mef (3,1%) concentrano il 68% delle allocazioni. L’attenzione alle misure anti Covid-19 è evidente con la maggiore concentrazione d’iniziative presentate dal ministero della salute, mentre le allocazioni per il settore ambientale, mantra trainante del Programma, non appaiono nelle prime 5 voci. Come mai la quota maggioritaria del Programma è destinata a misure anti Covid-19 quando i due binari del Programma ci portano verso transizione verde e digitale? Avanzo l’ipotesi che senza MES– osteggiato dal M5s– le risorse per la sanità hanno assorbito la maggior parte dell’ipotetico budget a discapito della crescita. Una coerenza non proprio cristallina con il Programma;

### Speculazioni sui fondi agli enti locali
> Nella lista dei progetti analizzati, non compaiono progetti presentati da regioni, città metropolitane e comuni. Ipotizziamo una scelta strategica del Mef: i fondi Sie ordinari che si liberano grazie al Recovery, potrebbero essere allocati per regioni, città metropolitano e comuni, esiste un’altra lista che non abbiamo. Se così non fosse, domani, regioni, città metropolitane e comuni dovrebbero scendere in piazza.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *5 novembre 2020*