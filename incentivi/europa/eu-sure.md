# Piano SURE

- [Descrizione](#descrizione)
- [Timeline](#timeline)

## Descrizione
Il **[piano SURE](https://ec.europa.eu/info/business-economy-euro/economic-and-fiscal-policy-coordination/financial-assistance-eu/funding-mechanisms-and-facilities/sure_it)** (temporary **S**upport to mitigate **U**nemployment **R**isks in an **E**mergency) è un piano a sostegno della cassa integrazione dei singoli stati.

L'Italia da questo programma riceverà in totale **€ 27,4 miliardi**.

> Tutti i soldi ricevuti dal piano SURE sono dei prestiti e quindi dovranno essere restituiti dai singoli stati. Tuttavia i tassi di interesse applicati agli stati sono uguali a quelli emessi dalla commissione, che nel caso dei bond decennali sono pari al -0,7%, quindi in realtà c'è un guadagno sui prestiti.

Interessante notare come l'Italia sia il maggior beneficiario da questo piano.

*Quote SURE*
![Quote SURE](assets/quote_sure.jpg)

## Timeline
**20 ottobre 2020** -> Emessi i primi titoli obbligazionari comuni a sostegno di questa manovra. Questi titoli sono stati assicurati come *social bonds*, ovvero titoli che avranno un rilevante impatto sociale.

**27 ottobre 2020** -> Emessi i primi 10 miliardi all'Italia per il piano SURE.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*