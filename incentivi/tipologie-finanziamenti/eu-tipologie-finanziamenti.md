# [Tipologie di finanziamenti / Europa](https://ec.europa.eu/info/funding-tenders/how-eu-funding-works/types-funding_it)

- [Sovvenzioni](#sovvenzioni)
- [Prestiti, garanzie e partecipazioni](#prestiti-garanzie-e-partecipazioni)
- [Sussidi](#sussidi)
- [Premi](#premi)
- [Appalti pubblici](#appalti-pubblici)

## Sovvenzioni
L’Unione europea assegna sovvenzioni alle organizzazioni e, talvolta, a singoli individui, per aiutarli a realizzare progetti che contribuiscono alle sue politiche. Le sovvenzioni vengono concesse in molti campi diversi, dalla ricerca all'istruzione o agli aiuti umanitari.

L’UE finanzia raramente progetti fino al 100%. Piuttosto, le sovvenzioni sono una forma complementare di finanziamento e l’organizzazione beneficiaria dovrà contribuire a sua volta con una percentuale al finanziamento del suo progetto.

La Commissione europea pubblicizza le opportunità di ricevere sovvenzioni attraverso inviti a presentare proposte.

## Prestiti, garanzie e partecipazioni
L’UE fornisce prestiti, garanzie e capitale come forme di assistenza finanziaria per sostenere le sue politiche e i suoi programmi. Ad esempio, l’UE eroga prestiti ad imprese di tutti i tipi per investimenti nella ricerca e nell’innovazione. Inoltre, offre garanzie per aiutare i beneficiari a ottenere prestiti più facilmente o a condizioni più favorevoli da banche e altri finanziatori. L'UE può inoltre partecipare finanziariamente a un progetto possedendone una parte.

## Sussidi
I sussidi sono gestiti direttamente dai governi nazionali dell’UE, e non dalla Commissione europea. Ad esempio, i sussidi all’agricoltura sono concessi per sostenere gli agricoltori.

## Premi
I premi sono ricompense destinate ai vincitori di concorsi banditi dal programma [Orizzonte 2020](http://ec.europa.eu/research/participants/docs/h2020-funding-guide/prizes/prizes_en.htm). La ricompensa ricevuta dal vincitore di un concorso può consistere in denaro, pubblicità o promozione.

## Appalti pubblici
La Commissione acquista beni, opere e servizi sul mercato per uso interno attraverso appalti pubblici. Essi sono selezionati tramite bandi di gara e non sono considerati una forma di finanziamento dell’UE.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *5 novembre 2020*
