# Tipologie di finanziamenti / Italia

- [Credito di imposta](#credito-di-imposta)

## Credito di imposta
Il credito d’imposta è un qualsiasi credito che il contribuente (nel nostro caso specifico l’azienda) vanta nei confronti dello Stato. Il credito d’imposta può essere utilizzato per compensare eventuali debiti dell’azienda nei confronti dell’erario, per il pagamento dei tributi e, quando ammesso, se ne può chiedere il rimborso nella dichiarazione dei redditi.

Il credito d’imposta lo si può avere anche nei confronti di altri enti pubblici come le Regioni, il Comune, Inail, Inps e altri soggetti di diritto pubblico.

> ??? - Verificare se il credito di imposta può essere riscosso anche con le dogane

**Esempio**\
Credito d’imposta pari al 30% fino a spese per 700.000 euro.

Se l’azienda “X” ha speso 540.000 euro per beni che rientrano tra le “spese ammissibili” del bando, allora vuol dire che ottiene un credito d’imposta pari a 162.000 euro (il 30% di 540.000) da utilizzare per scontare vecchi debiti, abbassare le tasse oppure può chiedere un rimborso nella dichiarazione dei redditi.

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *5 novembre 2020*