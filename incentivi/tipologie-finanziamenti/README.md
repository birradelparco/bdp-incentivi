# Tipologie di finanziamenti

## [Europa](eu-tipologie-finanziamenti.md)
L’UE mette a disposizione finanziamenti sotto varie forme, tra cui sovvenzioni, prestiti e garanzie, sussidi, premi e appalti pubblici.

## [Italia](it-tipologie-finanziamenti.md)
L'Italia utilizza vari strumenti finanziari e contributivi per rilasciare i fondi alle imprese.