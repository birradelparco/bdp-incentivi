# Articoli interessanti

- [Comunicazione COVID/Green](#comunicazione-covid-green)
- [Europa](#europa)

## Comunicazione COVID/Green

- [Paolo Iabichino - Le parole infette](https://medium.com/@Iabicus/le-parole-infette-c8a031b94302)
- [Nasce il Newtrain manifesto](https://startupitalia.eu/119649-20191215-nasce-il-newtrain-manifesto-poalo-iabichino-30-tesi-una-per-ogni-anno-che-ci-separa-dal-2050)
- **[L'ultimo treno / Newtrai](https://medium.com/@Iabicus/lultimo-treno-8f75f24ec226)**
- [Mega Trends](https://toptrends.nowandnext.com/wp-content/uploads/2017/05/Mega-trends-LR.jpg)
- [Earth speaker](https://www.adsoftheworld.com/media/digital/earth_speakr)
- [Finding words for crisis](https://thecreativeindependent.com/people/environmental-activist-jon-leland-on-finding-the-words-for-crisis-and-making-the-planet-feel-personal/)
- [Redesigning economics based on ecology](https://blog.usejournal.com/redesigning-economics-based-on-ecology-35a82f751f25)

## Europa
- [Limes - Next Generation EU = 18 piani Marshall](https://www.limesonline.com/italia-sfida-recovery-fund-eu-conte-spesa/120713)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*