# Condivisione eventi

- Scansione del QR Code sulle locandine
- Newsletter mensile con riepilogo
- Iscrizione al calendario sincronizzato online (WebCal protocol + file .ics)
- Notifiche push via nuovo sito web
	- [Service Workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API)
	- [Notification API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API/Using_the_Notifications_API)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*