# Mappe digitali personalizzate

- [Mapbox](https://www.mapbox.com)
- [OpenStreetMap](https://www.openstreetmap.org/#map=6/42.088/12.564)
- [Esempio di utilizzo](https://independentspaceindex.at/map)

---

Editor: *Tommaso Negri*\
Ultimo aggiornamento: *4 novembre 2020*